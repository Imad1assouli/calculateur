public class calculateur {
    public float number1;
    public float number2;

    public calculateur(float number1, float number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public calculateur() {
    }

    public float getNumber1() {
        return number1;
    }

    public float getNumber2() {
        return number2;
    }

    public void setNumber1(float number1) {
        this.number1 = number1;
    }

    public void setNumber2(float number2) {
        this.number2 = number2;
    }
    float Addit (){
        return this.getNumber1()+ this.getNumber2();
    }

    float Soustract (){
        return this.getNumber1()-this.getNumber2();
    }

    public float multiplication(float number1,float number2){
        return this.number1*this.number2;
    }

    public float division(float number1,float number2){
        return (this.number1/this.number2);
    }


}
